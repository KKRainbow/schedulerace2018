﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Gurobi;
using System.Threading;
using ShellProgressBar;

namespace schedule_race_c_
{
    class DataReader
    {
        public static int machLimit = int.MaxValue;
        public static int appLimt = int.MaxValue;
        public static DataTable ReadAppResource(string filename = "data/app_resources.csv")
        {
            DataTable table = new DataTable("app_resource");
            string[] lines = File.ReadAllLines(filename);
            table.Columns.AddRange(new DataColumn[]{
                new DataColumn("id", typeof(string)),
                new DataColumn("cpu_s", typeof(List<double>)),
                new DataColumn("memory_s", typeof(List<double>)),
                new DataColumn("disk", typeof(double)),
                new DataColumn("p", typeof(double)),
                new DataColumn("m", typeof(double)),
                new DataColumn("pm", typeof(double)),
            });
            table.PrimaryKey = new DataColumn[] { table.Columns["id"] };
            foreach (string line in lines)
            {
                string[] items = line.Split(",");
                table.Rows.Add(
                    items[0],
                    items[1].Split("|").Select(double.Parse).ToList(),
                    items[2].Split("|").Select(double.Parse).ToList(),
                    double.Parse(items[3]),
                    double.Parse(items[4]),
                    double.Parse(items[5]),
                    double.Parse(items[6])
                );
                if (table.Rows.Count > appLimt) break;
            }
            return table;
        }

        public static DataTable ReadMachineResources(string filename = "data/machine_resources.csv")
        {
            DataTable table = new DataTable("machine_resource");
            string[] lines = File.ReadAllLines(filename);
            table.Columns.AddRange(new DataColumn[]{
                new DataColumn("id", typeof(string)),
                new DataColumn("cpu", typeof(double)),
                new DataColumn("memory", typeof(double)),
                new DataColumn("disk", typeof(double)),
                new DataColumn("p", typeof(double)),
                new DataColumn("m", typeof(double)),
                new DataColumn("pm", typeof(double)),
            });
            table.PrimaryKey = new DataColumn[] { table.Columns["id"] };
            foreach (string line in lines)
            {
                string[] items = line.Split(",");
                table.Rows.Add(
                    (new object[]{
                        items[0],
                    }.Concat(
                        items.Skip(1).Select(item => (object)double.Parse(item)).ToList())
                    ).ToArray()
                );
                if (table.Rows.Count > machLimit) break;
            }
            return table;
        }

        public static DataTable ReadAppInterference(string filename = "data/app_interference.csv", DataTable ar = null)
        {
            DataTable table = new DataTable("app_interference");
            string[] lines = File.ReadAllLines(filename);
            table.Columns.AddRange(new DataColumn[]{
                new DataColumn("app_id1", typeof(string)),
                new DataColumn("app_id2", typeof(string)),
                new DataColumn("limit", typeof(int)),
            });
            foreach (string line in lines)
            {
                string[] items = line.Split(",");
                List<object> arr = new List<object> { items[0] };
                if (ar != null) {
                    if (!ar.Rows.Contains((string)items[0]) || !ar.Rows.Contains((string)items[1]))
                    {
                        continue;
                    }
                }
                table.Rows.Add(
                    items[0],
                    items[1],
                    int.Parse(items[2])
                );
            }
            return table;
        }

        public static DataTable ReadInstanceDeploy(string filename = "data/instance_deploy.csv")
        {
            DataTable table = new DataTable("instance_deploy");
            string[] lines = File.ReadAllLines(filename);
            table.Columns.AddRange(new DataColumn[]{
                new DataColumn("instance_id", typeof(string)),
                new DataColumn("app_id", typeof(string)),
                new DataColumn("machine_id", typeof(string)),
            });
            table.PrimaryKey = new DataColumn[] { table.Columns["instance_id"] };
            foreach (string line in lines)
            {
                string[] items = line.Split(",");
                List<object> arr = new List<object> { items[0] };
                table.Rows.Add(
                    items
                );
            }
            return table;
        }
    }
    class SchedulerData
    {
        private DataTable id, ar, mr, ai, varTable;
        private Dictionary<string, GRBVar> utilizationMap = new Dictionary<string, GRBVar>();
        private Dictionary<string, GRBVar[]> timesliceMap = new Dictionary<string, GRBVar[]>();
        GRBModel model;
        public SchedulerData(GRBModel m, int machLimit = int.MaxValue, int appLimit = int.MaxValue)
        {
            this.model = m;
            DataReader.machLimit = machLimit;
            DataReader.appLimt = appLimit;
            id = DataReader.ReadInstanceDeploy();
            ar = DataReader.ReadAppResource();
            mr = DataReader.ReadMachineResources();
            ai = DataReader.ReadAppInterference(ar:ar);
            varTable = this.BuildVarTable();
        }

        public DataRow[] GetAppInterferences()
        {
            var arr = new DataRow[ai.Rows.Count];
            ai.Rows.CopyTo(arr, 0);
            return arr;
        }

        public DataTable GetMachineTable()
        {
            return mr;
        }

        public Dictionary<string, DataRow> GetInstanceAppMap()
        {
            var tmp = from x in id.Select().AsEnumerable()
                      join y in ar.Select().AsEnumerable() on x["app_id"] equals y["id"]
                      select new { instance_id = x["instance_id"], app = y };
            var dict = tmp.ToDictionary(i => (string)i.instance_id, i => i.app);
            return dict;
        }

        public Dictionary<string, int> GetAppInstanceCountMap()
        {
            var tmp = from x in id.Select().AsEnumerable()
                      join y in ar.Select().AsEnumerable() on x["app_id"] equals y["id"]
                      group x by x["app_id"] into g
                      select new { app_id = (string)g.Key, count = g.Count() };
            var dict = tmp.ToDictionary(i => i.app_id, i => i.count);
            return dict;
        }
        public Dictionary<string, List<DataRow>> GetAppInstanceMap()
        {
            var tmp = from x in id.Select().AsEnumerable()
                      join y in ar.Select().AsEnumerable() on x["app_id"] equals y["id"]
                      group x by x["app_id"] into g
                      select new { app_id = (string)g.Key, list = g };
            var dict = tmp.ToDictionary(i => i.app_id, i => i.list.ToList());
            return dict;
        }

        public DataRow GetMachineResource(string mach_id)
        {
            return mr.Rows.Find(mach_id);
        }
        public DataRow GetAppResource(string app_id)
        {
            return ar.Rows.Find(app_id);
        }
        public int GetTimeSliceNum()
        {
            return ((List<double>)ar.Rows[0]["cpu_s"]).Count;
        }
        public int GetAppResourceCount()
        {
            return GetAllAppIds().Count;
        }
        public int GetMachResourceCount()
        {
            return GetAllMachineIds().Count;
        }


        private DataTable BuildVarTable()
        {
            //      app1 app2 app3
            // mach1
            // mach2
            // mach3
            DataTable table = new DataTable("var_table");
            var appIds = from x in ar.Select().AsEnumerable() select new DataColumn((string)x["id"], typeof(Gurobi.GRBVar));
            table.Columns.Add("machine_id");
            table.Columns.AddRange(appIds.ToArray());

            table.PrimaryKey = new DataColumn[] { table.Columns["machine_id"] };

            var machs = from x in mr.Select().AsEnumerable() select x["id"];
            
            foreach (var mach in machs)
            {
                var lb = Enumerable.Repeat((double)0, table.Columns.Count - 1).ToArray();
                var ub = Enumerable.Repeat((double)1e7, table.Columns.Count - 1).ToArray();
                var types = Enumerable.Repeat(GRB.INTEGER, table.Columns.Count - 1).ToArray();
                var p = (new object[] { mach }).Concat(
                    model.AddVars(lb, ub, null, types, null)
                ).ToArray();
                table.Rows.Add(p);

                timesliceMap.Add(
                    (string)mach,
                    model.AddVars(Enumerable.Repeat(0.0, GetTimeSliceNum()).ToArray(),
                                  Enumerable.Repeat(1.0, GetTimeSliceNum()).ToArray(),
                                  Enumerable.Repeat(1.0, GetTimeSliceNum()).ToArray(),
                                  Enumerable.Repeat(GRB.CONTINUOUS, GetTimeSliceNum()).ToArray(),
                                  Enumerable.Repeat("", GetTimeSliceNum()).ToArray())
                    );
            }
            return table;
        }

        public GRBVar GetVar(string app_id, string mach_id)
        {
            return (GRBVar)varTable.Rows.Find(mach_id)[app_id];
        }

        public void ResultDump()
        {
            int totalDeployedInstnace = 0;
            double machineUtilizationVar = 0;
            double machineUtiCount = 0;
            foreach(var mach in GetAllMachineIds())
            {
                machineUtilizationVar = GetMachineUtilizationVar(mach,0).X;
                if(machineUtilizationVar!=0)
                {
                    machineUtiCount += 1;
                }
                foreach(var app in GetAllAppIds())
                {
                    var grbvar = GetVar(app as string,mach as string);
                     if(grbvar.X>0)
                    {
                        totalDeployedInstnace += (int)grbvar.X;
                        string resstr = String.Format("{0,-13}->  {1,-10}:{2,-5}", mach,app,grbvar.X);
                        Console.WriteLine(resstr);
                    }
                }
            }
            Console.WriteLine("Total instances number:"+ GetInstanceAppMap().Keys.Count);
            Console.WriteLine("Total deployed instances number:" + totalDeployedInstnace);
            Console.WriteLine("Tatol machine number:" + mr.Rows.Count);
            Console.WriteLine("Machine ultilized count:" + machineUtiCount);
            Console.WriteLine("Tatol interference number:" + ai.Rows.Count);
            Console.WriteLine("Tatol app number:" + ar.Rows.Count);
        }
        public  void ReaultDumpToFile(string filePath="/home/yby/result.csv")
        {
            var resstr = new List<string>();
            foreach(var mach in GetAllMachineIds())
            {
                foreach(var app in GetAllAppIds())
                {
                    var grbvar = GetVar(app as string,mach as string);
                    resstr.Add(String.Format("{0},{1},{2}", mach, app, grbvar.X));
                }
            }
            File.WriteAllLines(filePath, resstr);
        }
        // key: mach_id, val: gurobi var
        public Dictionary<string, GRBVar> GetAllVarOfApp(string app_id)
        {
            var view = new DataView(varTable);
            var mach_table = view.ToTable(false, new string[] { "machine_id", app_id });
            var result = mach_table.Select().ToDictionary(
                    item => (string)item["machine_id"],
                    item => (GRBVar)item[app_id]
                 );
            return result;
        }

        // key: app_id, val: gurobi var
        public Dictionary<string, GRBVar> GetAllVarOfMach(string mach_id)
        {
            var row = varTable.Rows.Find(mach_id);
            var result = new Dictionary<string, GRBVar>();
            for (int i = 1; i < varTable.Columns.Count - 1; i++)
            {
                var column = varTable.Columns[i];
                result.Add(column.ColumnName, (GRBVar)row[column.Ordinal]);
            }
            return result;
        }

        public GRBVar GetMachineUtilizationVar(string mach_id,int time)
        {
            //return utilizationMap[mach_id];
            return timesliceMap[mach_id][time];
        }

        public List<string> GetAllMachineIds()
        {
            return mr.Select().AsEnumerable().Select(item => (string)item["id"]).ToList();
        }
        public List<string> GetAllAppIds()
        {
            return ar.Select().AsEnumerable().Select(item => (string)item["id"]).ToList();
        }
    }

    class ModelConstrainGenerator
    {
        private SchedulerData scheduleData;
        private GRBEnv env;
        private GRBModel model;
        private Object grbLock = new Object();
        
        private  const int BIGM = int.MaxValue;
        public ModelConstrainGenerator(SchedulerData data, GRBModel model)
        {
            scheduleData = data;
            this.model = model;
            // env = model.GetEnv();
        }
        public void GenerateAppInstNumConstrain(string app_id)
        {
            int instCount = (int)scheduleData.GetAppInstanceCountMap()[app_id];
            var varMachList = scheduleData.GetAllVarOfApp(app_id).Values;

            var varCount = varMachList.Count();
            double[] coeffes = Enumerable.Repeat((double)1, varCount).ToArray();

            //generate the GRBexpr
            var linExpr = new GRBLinExpr();
            linExpr.AddTerms(coeffes, varMachList.ToArray());

            lock (grbLock)
            {
                model.AddConstr(linExpr==instCount,"");
            }
        }

        public void GenerateResourceConstrains(string mach_id)
        {

            List<GRBTempConstr> constrainsList = new List<GRBTempConstr>();

            var allAppOfMach = scheduleData.GetAllVarOfMach(mach_id);
            var appCount = allAppOfMach.Count();
            double[] coeffs = new double[appCount];
            
            List<GRBVar> grbVarList = new List<GRBVar>();
            List<DataRow> appList = new List<DataRow>();

            GRBLinExpr tmpExpr = null;
            // get all app  GRBVar into a list
            foreach(var appId in allAppOfMach.Keys)
            {
                grbVarList.Add(allAppOfMach[appId]);
                appList.Add(scheduleData.GetAppResource(appId));
            }
            var grbArray = grbVarList.ToArray();
            var machineResource = scheduleData.GetMachineResource(mach_id) as DataRow;

            // for resource P, fill the coeffective value
            int i = 0;
            foreach(var app in appList)
            {
                coeffs[i] = (double)app["p"]; 
                i++;
            }
            double pMach = (double)machineResource["p"];
            tmpExpr = new GRBLinExpr();
            tmpExpr.AddTerms(coeffs,grbArray);
            constrainsList.Add(tmpExpr<=pMach);

            //for resource M, fill the coeefective value
            i = 0;
            foreach(var app in appList)
            {
                coeffs[i] = (double)app["m"]; 
                i++;
            }
            double mMach = (double)machineResource["m"];
            tmpExpr = new GRBLinExpr();
            tmpExpr.AddTerms(coeffs,grbArray);
            constrainsList.Add(tmpExpr<=mMach);

            //for resource PM, fill the coeefective value
            i = 0;
            foreach(var app in appList)
            {
                coeffs[i] = (double)app["pm"]; 
                i++;
            }
            double pmMach = (double)machineResource["pm"];
            tmpExpr = new GRBLinExpr();
            tmpExpr.AddTerms(coeffs,grbArray);
            constrainsList.Add(tmpExpr<=pmMach);

            //for resource disk, fill the coeefective value
            i = 0;
            foreach(var app in appList)
            {
                coeffs[i] = (double)app["pm"]; 
                i++;
            }
            double diskMach = (double)machineResource["disk"];
            tmpExpr = new GRBLinExpr();
            tmpExpr.AddTerms(coeffs,grbArray);
            constrainsList.Add(tmpExpr<=diskMach);

            
            double cpuMach = (double)machineResource["cpu"];
            double memMach = (double)machineResource["memory"];
            double cpuRate = 1.0/cpuMach;
            // get the time slice number
            for(int t=0;t<scheduleData.GetTimeSliceNum();t++)
            {
                //for resource cpu_s, fill the coeefective value
                i = 0;
                foreach(var app in appList)
                {
                    coeffs[i] = (double)((app["cpu_s"] as List<double>)[t]); 
                    i++;
                }
                tmpExpr = new GRBLinExpr();
                tmpExpr.AddTerms(coeffs,grbArray);
                constrainsList.Add(tmpExpr-cpuMach<=0);

                //for optimal utilziation objective variable constrain
                var utilizationVar = scheduleData.GetMachineUtilizationVar(mach_id,t);
                constrainsList.Add(tmpExpr*cpuRate<=utilizationVar);

                //for resource memory_s, fill the coeefective value
                i = 0;
                foreach(var app in appList)
                {
                    coeffs[i] = (double)((app["memory_s"] as List<double>)[t]);
                    i++;
                }
                tmpExpr = new GRBLinExpr();
                tmpExpr.AddTerms(coeffs,grbArray);
                constrainsList.Add(tmpExpr<=memMach);
            }
            lock(grbLock) 
            { 
                foreach(var cons in constrainsList)
                {
                    model.AddConstr(cons,"");
                }
            }
        }
        public void GenerateAppInterferenceConstrains(DataRow app_interference)
        {
            //inteference is specified as (appA,appB,k)
            var appAId = (string)app_interference["app_id1"];
            var appBId = (string)app_interference["app_id2"];
            int limit = (int)app_interference["limit"];

            List<GRBTempConstr> constrList = new List<GRBTempConstr>();

            foreach(var machId in scheduleData.GetAllMachineIds())
            {
                var appAVar = scheduleData.GetVar(appAId,machId) as GRBVar;
                var appBVar = scheduleData.GetVar(appBId,machId) as GRBVar;

                if(Object.ReferenceEquals(appAVar,appBVar))
                {
                    constrList.Add(appAVar<=limit);
                }
                else
                {
                    var z = model.AddVar(0, 1, 1, GRB.BINARY, "");
                    constrList.Add(appAVar-z*BIGM <= 0);
                    constrList.Add(appBVar+(z-1)*BIGM-limit<= 0);
                }
            }

            lock(grbLock)
            {
                foreach(var x in constrList)
                {
                    model.AddConstr(x, "");
                }
            }
        }
        public void SetObjective(string mach_id)
        {
            // objective function s_ij
            // s_ij = 1 + 10*(e^{max(0,u-0.5)-1) 
            double[] x_point = {
                0.0, 0.005, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63,
                0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 
                0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 
                0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0
             };
            double[] y_point = {
                0, 1, 1, 1.1005, 1.202, 1.3045, 1.4081, 1.5127, 1.6184, 1.7251, 1.8329, 1.9417, 2.0517, 2.1628, 
                2.275, 2.3883, 2.5027, 2.6183, 2.7351, 2.853, 2.9722, 3.0925, 3.214, 3.3368, 3.4608, 3.586, 3.7125, 
                3.8403, 3.9693, 4.0996, 4.2313, 4.3643, 4.4986, 4.6343, 4.7713, 4.9097, 5.0495, 5.1907, 5.3333, 5.4773, 
                5.6228, 5.7698, 5.9182, 6.0682, 6.2196, 6.3726, 6.5271, 6.6831, 6.8407, 6.9999, 7.1607, 7.3232, 7.4872
            };
            for(int t=0;t<scheduleData.GetTimeSliceNum();t++)
            {
                var s_tj = scheduleData.GetMachineUtilizationVar(mach_id,t);
                model.SetPWLObj(s_tj,x_point,y_point);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {  
            var env = new GRBEnv();
            var model = new GRBModel(env);
            //var data = new SchedulerData(model, appLimit: 100,machLimit:4000);
            var data = new SchedulerData(model,appLimit:100);
            var gen = new ModelConstrainGenerator(data, model);

            var options = new ProgressBarOptions 
            {
                ProgressCharacter = '─',
                ProgressBarOnBottom = true,
                DisplayTimeInRealTime = true
            };

            model.Parameters.NodefileDir="/home/yby/node/";
            model.Parameters.NodefileStart=0.5;
            model.Parameters.Threads = 12;
            model.Parameters.IgnoreNames = 1;

            model.Parameters.MIPFocus = 1; //for finding a feasible solution quickly
            //model.Parameters.ImproveStartGap = 0.1; //terminate when gap <= 10%
            model.Parameters.MIPGapAbs= 0.1; //terminate when gap <= 10%
            model.Parameters.MIPGap= 0.1; //terminate when gap <= 10%
            model.Parameters.NumericFocus = 0; //terminate when gap <= 10%
            //model.Parameters.NodeLimit = 10;

            model.ModelSense = GRB.MINIMIZE;

            var bar = new ProgressBar(data.GetMachResourceCount(), "Generating App Resource Constrains", options);
            Parallel.ForEach(data.GetAllMachineIds(), mach_id =>
            {
                gen.GenerateResourceConstrains(mach_id);
                bar.Tick();
            });

            bar = new ProgressBar(data.GetAppResourceCount(), "Add App Number constrains", options);
            Parallel.ForEach(data.GetAllAppIds(), app_id =>
            {
                gen.GenerateAppInstNumConstrain(app_id);
                bar.Tick();
            });

            var ais = data.GetAppInterferences();
            bar = new ProgressBar(ais.Count(), "Add App interference constrains", options);
            foreach(var ai in ais)
            {
                gen.GenerateAppInterferenceConstrains(ai);
                bar.Tick();
            }

            bar = new ProgressBar(data.GetMachResourceCount(), "setting objective variables", options);
            foreach(var mach_id in data.GetAllMachineIds())
            {
                gen.SetObjective(mach_id);
            }
            model.Optimize();
            if(model.Status==GRB.Status.OPTIMAL){
                data.ResultDump();
                data.ReaultDumpToFile();
            }
            return;
        }
    }
}

